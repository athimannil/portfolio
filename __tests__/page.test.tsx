import { render } from '@testing-library/react';
import Home from '@/app/page';

describe('Home page', () => {
  it('should render snaps', () => {
    const { asFragment } = render(<Home />);
    expect(asFragment()).toMatchSnapshot();
  });
});
