import { FC } from 'react';
import { BandTitleProps } from './types';

import './style.scss';

const BandTitle: FC<BandTitleProps> = ({ title }) => {
  if (title) {
    return <h2 className="bandtitle">{title}</h2>;
  }
  return null;
};

export default BandTitle;
