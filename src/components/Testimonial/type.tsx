export type TestimonialProps = {
  name: string;
  profileLink?: string | null;
  position: string;
  message: string;
};
