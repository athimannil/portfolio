import { ChangeEvent } from 'react';

export type InputTypeVariant = 'text' | 'number' | 'email' | 'search' | 'password' | 'textarea'

export type InputProps = {
  label?: string
  disabled?: boolean
  type?: InputTypeVariant
  value?: string | number
  isValid?: boolean,
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void
};
