import { FC } from 'react';
import { InputProps } from './types';
import './style.scss';

const Input: FC<InputProps> = ({ type, label }) => {

  return (
    <div className="input">
      {
        type === 'textarea' ?
          <textarea
            className="input__field input__field-textarea"
            placeholder={label}
            required
          /> :
          <input
            name="name"
            className="input__field"
            type={type ? type  : 'text'}
            placeholder={label}
            required
          />
      }
      { label && <label className="input__label" htmlFor={label}>{label}</label> }
    </div>
  );
};

export default Input;
