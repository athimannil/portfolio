import { FC } from 'react';
import Image from 'next/image';
import './style.scss';

interface PortfolioThumbnailProps {
  responsive: boolean;
  image: string;
  title: string;
  stacks: string[];
}

const PortfolioThumbnail: FC<PortfolioThumbnailProps> = ({ image, title, responsive, stacks }) => {
  if (responsive) {
    return (
      <figure className="figure">
        <Image className="figure__image" src={`/screenshot/${image}`} width={370} height={250} alt={title} />
        {
          stacks?.length > 0 &&
          <figcaption className="figure__title"> {stacks.map((item: string) => <span key={item}>{item}</span>)}</figcaption>
        }
      </figure>
    );
  }
  return (
    <figure>
      <a href="#" target="_blank">
        <Image src={`/screenshot/${image}`} width={370} height={250} alt={title} />
      </a>
    </figure>
  );
};

export default PortfolioThumbnail;
