import { render, screen } from '@testing-library/react';
import Footer from './Footer';

describe('Footer component', () => {
  it('should click portfolio link', () => {
    render(<Footer />);

    const portfolioLink = screen.getByText(/© 2023 muhammed athimannil \- all right reserved/i);
    expect(portfolioLink).toBeInTheDocument();
  });

  it('footer snaps', () => {
    const { asFragment } = render(<Footer />);
    expect(asFragment()).toMatchSnapshot();
  });
});
