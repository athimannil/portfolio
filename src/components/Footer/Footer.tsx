import Image from 'next/image';

import './style.scss';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container flex justify-between items-end">
        <div className="footer__stack">
          <div className="footer__stack-icon">
            <Image className="max-h-8" src="./images/html5.svg" height={30} width={30} alt="html5" />
            <Image className="max-h-8" src="./images/css3.svg" height={30} width={30} alt="CSS3" />
            <Image className="max-h-8" src="./images/js.svg" height={30} width={30} alt="javascript" />
            <Image className="max-h-8" src="./images/sass.svg" height={30} width={30} alt="Sass" />
            <Image className="max-h-8" src="./images/gulp.svg" height={30} width={30} alt="gulp" />
            <Image className="max-h-8" src="./images/php.svg" height={30} width={30} alt="PHP" />
          </div>
          <p className="footer__stack-right">&copy; {new Date().getFullYear()} Muhammed Athimannil  - All right reserved</p>
        </div>
        <div className="footer__social">
          <p>Connect with me</p>
          <div className="footer__social-icon">
            <a className="stackoverflow" href="http://stackoverflow.com/users/884521/muhammed-athimannil" target="_blank">
              <Image src="./images/stack.svg" width={42} height={42} alt="stackoverflow" />
            </a>
            <a className="linkedin" href="http://uk.linkedin.com/in/msathimannil/" target="_blank">
              <Image src="./images/linkedin.svg" width={42} height={42} alt="linkedin" />
            </a>
            <a className="twitter" href="https://twitter.com/MSAthimannil" target="_blank">
              <Image src="./images/twitter.svg" width={42} height={42} alt="twitter" />
            </a>
            <a className="facebook" href="https://www.facebook.com/MSAthimannil" target="_blank">
              <Image src="./images/facebook.svg" width={42} height={42} alt="facebook" />
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
