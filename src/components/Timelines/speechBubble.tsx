import React, { FC } from 'react';
import { SpeechBubbleProps } from './type';

const SpaachBubble: FC<SpeechBubbleProps> = ({ title, company, location, description, active }) => {
  return (
    <section className="bubble">
      <div className="bubble__header">
        <div>
          <h3 className="bubble__header-title">{title}</h3>
          <h5 className="bubble__header-location">{location}</h5>
        </div>
        <h4 className="bubble__header-company">{company}</h4>
      </div>
      <div className={`bubble__body ${active ? 'bubble__body-active' : ''}`}>
        <p className="bubble__body-description">{description}</p>
      </div>
    </section>
  );
};

export default SpaachBubble;
