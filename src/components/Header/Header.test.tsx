import { fireEvent, render, screen } from '@testing-library/react';
import Header from './Header';

describe('Header component', () => {
  jest.mock('next/navigation', () => ({
    push: jest.fn()
  }));

  it('Click nav link ', () => {
    render(<Header />);

    const aboutLink = screen.getByRole('link', { name: /about me/i });
    fireEvent.click(aboutLink);
  });

  it('renders header snaps', () => {
    const { asFragment } = render(<Header />);
    expect(asFragment()).toMatchSnapshot();
  });
});
