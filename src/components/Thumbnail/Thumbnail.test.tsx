import { render, screen } from '@testing-library/react';
import Thumbnail from './Thumbnail';

describe('Thumbnail in various rendering', () => {
  it('Thumbnail with title', () => {
    render(<Thumbnail image='athimannil.jpeg' title="Muhammed Athimannils" />);

    const thumbnailImage = screen.getByRole('img', {
      name: /muhammed athimannils/i
    }) as HTMLImageElement;
    expect(thumbnailImage).toBeInTheDocument();

    const thumbnailCaption = screen.getByText(/muhammed athimannils/i);
    expect(thumbnailCaption).toBeInTheDocument();
  });

  it('Thumbnail without title', () => {
    render(<Thumbnail image="muhammed.jpeg" />);
    screen.logTestingPlaygroundURL();

    const thumbnailImage = screen.getByRole('img') as HTMLImageElement;
    expect(thumbnailImage).toBeInTheDocument();
  });

  it('render thumbnail snaps', () => {
    const { asFragment } = render(<Thumbnail image='athimannil.jpeg' title="Muhammed Athimannils" />);
    expect(asFragment()).toMatchSnapshot();
  });
});
