import { ReactNode } from 'react';
import './globals.scss';
import { Inter } from 'next/font/google';
import Header from '@/components/Header';
import Footer from '@/components/Footer';

const inter = Inter({ subsets: ['latin'] });

export const metadata = {
  title: 'MosApp',
  description: 'MosApp in Next.JS',
};

export default function RootLayout({
  children,
}: {
  children: ReactNode
}) {
  return (
    <html lang="en">
      <body className="min-h-screen flex flex-col antialiased">
        <Header />
        <main className={`flex-1 ${inter.className}`}>
          {children}
        </main>
        <Footer />
      </body>
    </html>
  );
}
