import Thumbnail from '@/components/Thumbnail';

import './style.scss';
import BandTitle from '@/components/BandTitle/BandTitle';
import Testimonial from '@/components/Testimonial';
import Timelines from '@/components/Timelines';

import cvData from './cvData.json';

const { experience, education, testimonials } = cvData;

const Resume = () => {
  return (
    <>
      <div className="cv">
        <section className="cvcontact">
          <address className="cvcontact__address">
            <h1 className="cvcontact__address-name">Muhammed Athimannil</h1>
            <h3 className="cvcontact__address-title">Frontend Developer</h3>
            <hr className="cvcontact__address-border" />
            <h3 className="cvcontact__address-location">Berlin<br />Germany</h3>
            <p className="cvcontact__address-number">m: <a className="cvcontact__address-number--link" href="tel:+4915782119447">+49 (0)157 8211 9447</a></p>
            <p className="cvcontact__address-number">e: <a className="cvcontact__address-number--link" href="mailto:hello@athimannil.com">hello@athimannil.com</a></p>
          </address>
          <div className="cvcontact__thumbnail">
            <Thumbnail image="muhammed.jpg" title="Muhammed Athimannil" />
          </div>
        </section>
        <section className="summary">
          <BandTitle title="Personal profile" />
          <p className="summary__text">I am a technical enthusiast having outstanding skills and passion for Web Develop- ment. I find it really thrilling to collaborate designing & programming and to be a web developer. I have experience in developing Front-End and Back-End, with the goal of delivering cutting edge user interface.</p>
          <p className="summary__text">I am fond of implementing modern emerging technologies such as HTML5, CSS3, Responsive Design and different kinds of framework and integrating them together to keep cross browser compatibility graceful in degrades browsers. I strive to write codes of the highest standard and bug fix in an efficient and extensible way. I would like to have bright career in the same field where I can contribute my skills to the maximum.</p>
        </section>
        <section className="summary">
          <BandTitle title="Experience" />
          <Timelines items={experience} />
        </section>
        <section className="summary">
          <BandTitle title="Education" />
          <Timelines items={education} />
        </section>
        <section className="testimonials">
          <BandTitle title="Testimonials" />
          <div className="testimonials__comments">
            {
              testimonials.map(({ name, position, message, profileLink }) =>
                <Testimonial key={name} name={name} profileLink={profileLink} position={position} message={message} />
              )
            }
          </div>
        </section>
      </div>
    </>
  );
};

export default Resume;
