'use client';
import React, { FormEvent } from 'react';
import './style.scss';
import Input from '@/components/Input';
import LocationMap from '@/components/LocationMap';

const Contact = () => {
  const submitForm = (event: FormEvent) => {
    event.preventDefault();
    console.log('Thanks for message');
  };

  return (
    <>
      {/* <div id="my-map" className="map"></div> */}
      <LocationMap />
      {/* <section className="container mx-auto px-4 flex items-end mb-10"> */}
      <section className="container mt-20 mb-10 grid grid-cols-11 gap-x-8 grid-rows-7">
        <div className="form col-start-1 col-end-7 row-start-1 row-end-7">
          <h1 className="form__title">Contact</h1>
          <p className="form__description">Take a look around the site, Do you have something in mind you would like to let me know .You can contact me by filling the form below or email hello@athimannil.com</p>
          <form onSubmit={submitForm}>

            <Input type="text" label="Name" />
            
            <Input type="email" label="Email" />

            <Input type="textarea" label="Comment" />

            <input className="button form__button" type="submit" name="submit" value="Send" />
          </form>
        </div>

        <div className="flex col-start-7 col-end-12 row-start-5 row-end-5">
          <a className="contact" href="mailto:hello@athimannil.com">
            <span className="contact__icon">
              <svg className="contact__icon-envelop" width="55" height="33" viewBox="0 0 55 33" xmlns="http://www.w3.org/2000/svg">
                <g fill="#ffffff" fill-rule="evenodd">
                  <path className="contact__icon-open" d="M26.38 21.08L1.38.1h50l-25 20.98z" />
                  <path d="M17.52 16.534L0 31.237V1.83l17.52 14.704zM19.792 18.44l7.688 6.45 7.687-6.45 17.33 14.542H2.463L19.792 18.44zM37.438 16.534L54.958 1.83v29.407L37.44 16.534z" />
                </g>
              </svg>
            </span>
            <span className="contact__item">hello@athimannil.com</span>
          </a>
          <a className="contact" href="tel:+4915782119447">
            <span className="contact__icon">
              <svg className="contact__icon-mobile" width="32" height="60" viewBox="0 0 32 60" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 2.472C0 1.24 1.207 0 2.438 0h27.15C30.792 0 32 1.223 32 2.472v55.093C32 58.755 30.837 60 29.59 60c-1.25 0-25.914-.01-27.152 0C1.2 60.01 0 58.8 0 57.565V2.472zM16 57.5c1.36 0 2.462-1.12 2.462-2.5 0-1.382-1.103-2.5-2.462-2.5-1.36 0-2.462 1.118-2.462 2.5 0 1.38 1.103 2.5 2.462 2.5zM2.462 6.24h27.076V50H2.462V6.24z" fill="#ffffff" fill-rule="evenodd" />
              </svg>
            </span>
            <span className="contact__item">+91989531991</span>
          </a>
        </div>
      </section>
    </>
  );
};

export default Contact;
