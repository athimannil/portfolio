// import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';

import './style.scss';

const serviceList = [
  {
    image: 'screens.svg',
    title: 'who I am',
    description: 'I also have a keen interest in responsive web development and have recently started putting this into practice, as you can see by this very website.',
    link: 'about'
  },
  {
    image: 'cogs.svg',
    title: 'who I do',
    description: 'I create unique, clean sites that are easy to navigate. All my works comply with web standards, use the latest industry techniques especially HTM5 and CSS3.',
    link: 'portfolio'
  },
  {
    image: 'cups.svg',
    title: 'who I love',
    description: 'I love to create unique clear responsive sites that are user friendly. All of my works comply with web standards and elegant with hand coded.',
    link: 'about'
  },
];

const companiesList = [
  {
    image: 'bt.svg',
    name: 'British Telecom',
  }, {
    image: 'microsoft.svg',
    name: 'Microsoft',
  }, {
    image: 'ucl.svg',
    name: 'University College of London',
  }, {
    image: 'ba.svg',
    name: 'British Airways',
  }, {
    image: 'john-lewis.svg',
    name: 'John Lewis',
  }, {
    image: 'marriot.svg',
    name: 'Marriot',
  }, {
    image: 'ui-centric.svg',
    name: 'UI Centric',
  },
];

const Home = () => {
  return (
    <>
      <section className="bg-zinc-100 border-y border-zinc-200">
        <div className="introduction">
          {/* TODO font family helvatica */}
          <h1 className="introduction__title">Hello, I'm <span className="introduction__title-name">Muhammed Athimannil</span> and I am a creative freelance web developer based in <span className="introduction__title-location">Berlin</span></h1>
          <blockquote className="introduction__subheading">
            <p className="introduction__subheading-quote">&ldquo;Every nice creations start from imagination followed by dedication&rdquo;</p>
          </blockquote>
        </div>
      </section>

      <section className="services">
        {serviceList.map(({ image, title, description, link, }) =>
          <article className="services__item" key="title">
            <h2 className="services__item-title">{title}</h2>
            <Image src={`./images/${image}`} className="services__item-image" width={200} height={140} alt={image} />
            <p className="services__item-description">{description}</p>
            <Link className="services__item-button" href={`/${link}`}>Read more</Link>
          </article>
        )}
      </section>

      <section className="clients">
        <h2 className="clients__title">Teams I worked with</h2>
        <div className="clients__logos">
          {companiesList.map(({ name, image }) =>
            <img
              key={name}
              className="clients__logos-image"
              src={`./images/${image}`}
              alt={name}
            />
          )}
        </div>
      </section>
    </>
  );
};

export default Home;
